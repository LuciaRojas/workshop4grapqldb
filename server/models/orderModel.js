const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const product = require('./productModel');
const client = require('./clientModel');

const order = new Schema({
  client: client.schema,
  products: [product.schema],
});

module.exports = mongoose.model('orders', order);